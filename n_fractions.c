//WAP to find the sum of n fractions.
#include <stdio.h>

struct fraction
{
	int n;
	int d;
};
typedef struct fraction Fraction;

Fraction input()
{
	Fraction f;
	printf("Enter numerator: ");
	scanf("%d", &f.n);
	printf("Enter denominator: ");
	scanf("%d", &f.d);
	
	return f;
}

Fraction addition(Fraction f1, Fraction f2)
{
	Fraction res;
	res.d = f1.d * f2.d;
	res.n = (f1.n * f2.d) + (f2.n * f1.d);

	return res;
}

int gcd(int n, int d)
{
	int div;
	for (int i = 1; i <= n && i <= d; i++)
	{
		if (n % i == 0 && d % i == 0)
			div = i;
	}
	return div;
}

Fraction reduce_lowest(Fraction f, int divisor)
{
    f.n /= divisor;
	f.d /= divisor;

    return f;
}

Fraction total_sum(Fraction arr[], int size)
{
	Fraction total;
	total.n = 0;
	total.d = 1;

	for (int i = 0; i < size; i++)
		total = addition(total, arr[i]);
	
	int divisor = gcd(total.n, total.d);
	total = reduce_lowest(total, divisor);

	return total;
}

Fraction output(Fraction f)
{
	printf("The answer is: %d/%d", f.n, f.d);
}

int main()
{
	int n, divisor;
	Fraction result;

	printf("Enter the number of fractions: ");
	scanf("%d", &n);

	if (n == 1)
		result = input();
	else
	{
		Fraction fracs[n];

		for (int i = 0; i< n; i++)
			fracs[i] = input();

		result = total_sum(fracs, n);
	}
	output(result);

	return 0;
}